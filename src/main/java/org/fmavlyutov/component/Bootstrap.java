package org.fmavlyutov.component;

import org.fmavlyutov.api.ICommandController;
import org.fmavlyutov.api.ICommandRepository;
import org.fmavlyutov.api.ICommandService;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.controller.CommandController;
import org.fmavlyutov.repository.CommandRepository;
import org.fmavlyutov.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private ICommandRepository commandRepository = new CommandRepository();

    private ICommandService commandService = new CommandService(commandRepository);

    private ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        argumentsProcessing(args);
        commandsProcessing();
    }

    private void argumentsProcessing(String[] args) {
        commandController.displayHello();
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            switch (arg) {
                case CommandLineArgument.HELP:
                    commandController.displayHelp();
                    break;
                case CommandLineArgument.VERSION:
                    commandController.displayVersion();
                    break;
                case CommandLineArgument.ABOUT:
                    commandController.displayAbout();
                    break;
                case CommandLineArgument.INFO:
                    commandController.displayInfo();
                    break;
                case CommandLineArgument.COMMANDS:
                    commandController.displayCommands();
                    break;
                case CommandLineArgument.ARGUMENTS:
                    commandController.displayArguments();
                    break;
                default:
                    commandController.displayArgumentError();
            }
        }
    }

    private void commandsProcessing() {
        String arg = "";
        Scanner scanner = new Scanner(System.in);
        while (!arg.equals(CommandLineConstant.EXIT)) {
            arg = scanner.next();
            displayCommand(arg);
        }
    }

    private void displayCommand(String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case CommandLineConstant.HELP:
                commandController.displayHelp();
                break;
            case CommandLineConstant.VERSION:
                commandController.displayVersion();
                break;
            case CommandLineConstant.ABOUT:
                commandController.displayAbout();
                break;
            case CommandLineConstant.INFO:
                commandController.displayInfo();
                break;
            case CommandLineConstant.COMMANDS:
                commandController.displayCommands();
                break;
            case CommandLineConstant.ARGUMENTS:
                commandController.displayArguments();
                break;
            case CommandLineConstant.EXIT:
                exit();
                break;
            default:
                commandController.displayCommandError();
        }
    }

    private void exit() {
        commandController.displayGoodbye();
        System.exit(0);
    }

}
