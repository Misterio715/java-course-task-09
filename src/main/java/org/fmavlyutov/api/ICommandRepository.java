package org.fmavlyutov.api;

import org.fmavlyutov.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
